﻿using Newtonsoft.Json;
using NLog;

namespace AkTech.Logging.SqlProfiler.Storage
{
    /// <summary>
    /// 
    /// </summary>
    public class NlogStorage : IStorage
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger(); 
        /// <inheritdoc />
        public void Save(MiniProfiler profiler) //TODO
        {
            _logger.Log(new LogEventInfo()
            {
                Level = LogLevel.Info,
                Message = JsonConvert.SerializeObject(profiler)
            });
        }
    }
}
